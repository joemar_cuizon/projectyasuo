﻿Imports System.Data.OleDb
Public Class frmDelete
    Dim dbPath As String = Application.StartupPath & "\Products.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    Private Sub txtCode_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCode.KeyPress
        If e.KeyChar = vbCr Then
            Dim dbDataReader As OleDb.OleDbDataReader = Nothing
            Dim sqlCommand1 As String = "SELECT * FROM Inventory WHERE ProductCode = '" & txtCode.Text & "'"

            dbDataReader = performQuery(connectionString, sqlCommand1)
            If dbDataReader.HasRows Then
                While dbDataReader.Read()
                    txtName.Text = dbDataReader("ProductName".ToString)
                    txtDescription.Text = dbDataReader("ProductDescription".ToString)
                    txtQuantity.Text = dbDataReader("ProductQuantity".ToString)
                    txtCost.Text = dbDataReader("ProductCost".ToString)
                End While


            Else
                MessageBox.Show("Last Name Not Found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtCode.Focus()


                Exit Sub
            End If
        End If
    End Sub





    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Dim sqlCommand As String = "DELETE * FROM Inventory WHERE ProductCode = '" & txtCode.Text & "'"
        If performNonQuery(connectionString, sqlCommand) Then
            MessageBox.Show("A Product Has Been Deleted", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtCode.Clear()
            txtName.Clear()
            txtDescription.Clear()
            txtCost.Clear()
            txtQuantity.Clear()


            Exit Sub
        Else
            MessageBox.Show("Please Input An Available Product Code", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtCost.Focus()
            Exit Sub
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        time1.Text = Format(TimeOfDay)
        date2.Text = Format(Today)
    End Sub

    Private Sub frmDelete_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Enabled = True
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        txtCode.Clear()
        txtDescription.Clear()
        txtName.Clear()
        txtQuantity.Clear()
        txtCost.Clear()
        frmUpdate.Show()
        Me.Hide()

    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtCode.Clear()
        txtDescription.Clear()
        txtName.Clear()
        txtQuantity.Clear()
        txtCost.Clear()
        txtCode.Focus()
    End Sub
End Class