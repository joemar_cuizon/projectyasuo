﻿Imports System.Data.OleDb
Public Class frmSearch
    Dim dbPath As String = Application.StartupPath & "\Products.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    Private Sub txtSearch_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSearch.KeyPress
        If e.KeyChar = vbCr Then
            Dim Search As String = txtSearch.Text
            Dim dbDataReader As OleDb.OleDbDataReader = Nothing
            If rdbCode.Checked Then
                Dim sql As String = "SELECT * FROM Inventory Where ProductCode like '%" & Search & "%'"
                Dim connection As New OleDbConnection(connectionString)
                Dim dataadapter As New OleDbDataAdapter(sql, connection)
                Dim ds As New DataSet
                connection.Open()
                dataadapter.Fill(ds, "Inventory")
                connection.Close()
                dgvProducts.DataSource = ds
                dgvProducts.DataMember = "Inventory"
                dgvProducts.ReadOnly = True
                rdbCode.Checked = False
                txtSearch.Clear()

            ElseIf rdbName.Checked Then
                Dim Search1 As String = txtSearch.Text
                Dim sql As String = "SELECT * FROM Inventory Where ProductName like '%" & Search1 & "%'"
                Dim connection As New OleDbConnection(connectionString)
                Dim dataadapter As New OleDbDataAdapter(sql, connection)
                Dim ds As New DataSet
                connection.Open()
                dataadapter.Fill(ds, "Inventory")
                connection.Close()
                dgvProducts.DataSource = ds
                dgvProducts.DataMember = "Inventory"
                dgvProducts.ReadOnly = True
                rdbName.Checked = False
                txtSearch.Clear()

            ElseIf rdbDate.Checked Then
                Dim Search2 As String = txtSearch.Text
                Dim sql As String = "SELECT * FROM Inventory Where ProductDate like '%" & Search2 & "%'"
                Dim connection As New OleDbConnection(connectionString)
                Dim dataadapter As New OleDbDataAdapter(sql, connection)
                Dim ds As New DataSet
                connection.Open()
                dataadapter.Fill(ds, "Inventory")
                connection.Close()
                dgvProducts.DataSource = ds
                dgvProducts.DataMember = "Inventory"
                dgvProducts.ReadOnly = True
                rdbDate.Checked = False
                txtSearch.Clear()
            End If
        End If


    End Sub



    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged

    End Sub

    Private Sub btnAll_Click(sender As Object, e As EventArgs) Handles btnAll.Click
        Dim sql As String = "SELECT * FROM Inventory "
        Dim connection As New OleDbConnection(connectionString)
        Dim dataadapter As New OleDbDataAdapter(sql, connection)
        Dim ds As New DataSet
        connection.Open()
        dataadapter.Fill(ds, "Inventory")
        connection.Close()
        dgvProducts.DataSource = ds
        dgvProducts.DataMember = "Inventory"
        dgvProducts.ReadOnly = True
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        frmUpdate.Show()
        Me.Hide()

    End Sub
End Class