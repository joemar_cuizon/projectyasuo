﻿Imports System.Data.OleDb
Public Class frmChangePassword
    Dim dbPath As String = Application.StartupPath & "\Products.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles lblNew.Click

    End Sub
    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles txtNew.TextChanged

    End Sub

    Private Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click
        If txtOld.Text = My.Settings.login Then
            If txtNew.Text = txtNew2.Text Then
                My.Settings.login = txtNew.Text
                MessageBox.Show("You have Change your Password", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                frmLogin.Show()
                Me.Hide()
                ClearFields()
                My.Settings.Save()

            Else
                MessageBox.Show("Please Fill It Up Accordingly", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub txtOld_TextChanged(sender As Object, e As EventArgs) Handles txtOld.TextChanged

    End Sub
    Private Function ClearFields()
        txtNew.Clear()
        txtOld.Clear()
        txtNew2.Clear()
    End Function



    Private Sub menuHome_Click(sender As Object, e As EventArgs)
        frmSearch.Show()
        Me.Hide()
        Dim sql As String = "SELECT * FROM Inventory"
        Dim connection As New OleDbConnection(connectionString)
        Dim dataadapter As New OleDbDataAdapter(sql, connection)
        Dim ds As New DataSet
        connection.Open()
        dataadapter.Fill(ds, "Inventory")
        connection.Close()
        frmSearch.dgvProducts.DataSource = ds
        frmSearch.dgvProducts.DataMember = "Inventory"
        frmSearch.dgvProducts.ReadOnly = True

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        frmUpdate.Show()
        Me.Hide()
        ClearFields()

    End Sub
End Class