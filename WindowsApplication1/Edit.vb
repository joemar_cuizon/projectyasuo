﻿Imports System.Data.OleDb
Public Class frmEdit
    Dim dbPath As String = Application.StartupPath & "\Products.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        txtCode.Clear()
        txtCost.Clear()
        txtDescription.Clear()
        txtName.Clear()
        txtQuantity.Clear()
        frmUpdate.Show()
        Me.Hide()

    End Sub

    Private Sub txtCode_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCode.KeyDown

    End Sub
    Private Sub txtCode_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCode.KeyPress
        If e.KeyChar = vbCr Then
            Dim dbDataReader As OleDb.OleDbDataReader = Nothing
            Dim sqlCommand1 As String = "SELECT * FROM Inventory WHERE ProductCode = '" & txtCode.Text & "'"

            dbDataReader = performQuery(connectionString, sqlCommand1)
            If dbDataReader.HasRows Then
                While dbDataReader.Read()
                    txtName.Text = dbDataReader("ProductName".ToString)
                    txtDescription.Text = dbDataReader("ProductDescription".ToString)
                    txtQuantity.Text = dbDataReader("ProductQuantity".ToString)
                    txtCost.Text = dbDataReader("ProductCost".ToString)
                End While


            Else
                MessageBox.Show("Last Name Not Found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtCode.Focus()


                Exit Sub
            End If
        End If

    End Sub

    Private Sub txtCode_TextChanged(sender As Object, e As EventArgs) Handles txtCode.TextChanged

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        txtCost.Enabled = True
        txtDescription.Enabled = True
        txtName.Enabled = True
        txtQuantity.Enabled = True
        btnConfirm.Show()
        btnOK.Hide()
        btnEdit.Hide()


    End Sub

    Private Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click

        Dim sqlCommand As String = "UPDATE Inventory SET ProductTime = '" & time1.Text & "',ProductDate = '" & date2.Text & "',ProductQuantity = '" & txtQuantity.Text & "',ProductName = '" & txtName.Text & "', ProductDescription = '" & txtDescription.Text & "', ProductCost = '" & txtCost.Text & "' WHERE ProductCode = '" & txtCode.Text & "'"
        If performNonQuery(connectionString, sqlCommand) Then
            MessageBox.Show("Successful", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            btnEdit.Show()
            txtCost.Enabled = False
            txtDescription.Enabled = False
            txtName.Enabled = False
            txtQuantity.Enabled = False
            btnConfirm.Visible = False
            btnOK.Show()

            Exit Sub

        End If
    End Sub


    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        txtCode.Clear()
        txtCost.Clear()
        txtDescription.Clear()
        txtName.Clear()
        txtQuantity.Clear()
        frmUpdate.Show()
        Me.Hide()
    End Sub

    Private Sub frmEdit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        time1.Text = Format(TimeOfDay)
        date2.Text = Format(Today)
    End Sub

    Private Sub txtCost_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCost.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "1234567890 "
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar() = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCost_TextChanged(sender As Object, e As EventArgs) Handles txtCost.TextChanged

    End Sub

    Private Sub txtQuantity_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtQuantity.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "1234567890 "
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar() = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtQuantity_TextChanged(sender As Object, e As EventArgs) Handles txtQuantity.TextChanged

    End Sub
End Class