﻿Imports System.Data.OleDb
Public Class frmAdd
    Dim dbPath As String = Application.StartupPath & "\Products.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function



    Private Sub TxtCode_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtProduct.Text.Trim.Length = 0 Then
            MessageBox.Show("Please Input your Desired Product Code", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If txtName.Text.Trim.Length = 0 Then
            MessageBox.Show("Please Input your Desired Product Name", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If txtDescription.Text.Trim.Length = 0 Then
            MessageBox.Show("Please Input A Description", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If txtCost.Text.Trim.Length = 0 Then
            MessageBox.Show("Please Input Your Desired Product Cost", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


        If txtQuantity.Text.Trim.Length = 0 Then
            MessageBox.Show("Please Input Product Quantity", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub

        End If
        Dim sqlcommand As String



        sqlcommand = "INSERT INTO Inventory (ProductCode, ProductName, ProductDescription, ProductCost, ProductQuantity, ProductDate, ProductTime) VALUES ('" & txtProduct.Text & "', '" & txtName.Text & "', '" & txtDescription.Text & "','" & txtCost.Text & "', '" & txtQuantity.Text & "', '" & date1.Text & "', '" & time1.Text & "')"
        If performNonQuery(connectionString, sqlCommand) Then
            MessageBox.Show("Successfully Added.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            ClearFields()
            txtProduct.Focus()



            Exit Sub
        Else
            MessageBox.Show("Please Fill Up Completely", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub

        End If

    End Sub
    Private Function ClearFields()
        txtProduct.Clear()
        txtName.Clear()
        txtDescription.Clear()
        txtCost.Clear()
        txtQuantity.Clear()
        Return 0
    End Function

    Private Sub txtQuantity_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtQuantity.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "1234567890 "
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar() = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub


    Private Sub TxtQuantity_TextChanged(sender As Object, e As EventArgs) Handles txtQuantity.TextChanged

    End Sub


    Private Sub txtProduct_KeyDown(sender As Object, e As KeyEventArgs) Handles txtProduct.KeyDown

    End Sub

    Private Sub txtProduct_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtProduct.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "1234567890 "
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar() = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCost_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCost.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "1234567890 "
            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar() = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCost_TextChanged(sender As Object, e As EventArgs) Handles txtCost.TextChanged

    End Sub













    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs)
        frmSearch.Show()
        Me.Hide()
        Dim sql As String = "SELECT * FROM Inventory"
        Dim connection As New OleDbConnection(connectionString)
        Dim dataadapter As New OleDbDataAdapter(sql, connection)
        Dim ds As New DataSet
        connection.Open()
        dataadapter.Fill(ds, "Inventory")
        connection.Close()
        frmSearch.dgvProducts.DataSource = ds
        frmSearch.dgvProducts.DataMember = "Inventory"
        frmSearch.dgvProducts.ReadOnly = True
    End Sub

    Private Sub HistoryToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub ChangePasswordToolStripMenuItem_Click(sender As Object, e As EventArgs)
        frmChangePassword.Show()
        Me.Hide()


    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Me.Close()

    End Sub






    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtProduct.Clear()
        txtDescription.Clear()
        txtName.Clear()
        txtQuantity.Clear()
        txtCost.Clear()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        time1.Text = Format(TimeOfDay)
        date1.Text = Format(Today)
    End Sub

    Private Sub time1_Click(sender As Object, e As EventArgs) Handles time1.Click

    End Sub

    Private Sub frmAdd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Enabled = True

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        frmUpdate.Show()
        Me.Hide()
        txtCost.Clear()
        txtDescription.Clear()
        txtName.Clear()
        txtQuantity.Clear()
        txtProduct.Clear()

    End Sub

    Private Sub txtProduct_TextChanged(sender As Object, e As EventArgs) Handles txtProduct.TextChanged

    End Sub
End Class