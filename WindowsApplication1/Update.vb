﻿Imports System.Data.OleDb
Public Class frmUpdate
    Dim dbPath As String = Application.StartupPath & "\Products.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        frmDelete.Show()
        Me.Hide()

    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        frmAdd.Show()
        Me.Hide()

    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        frmEdit.Show()
        Me.Hide()

    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Me.Close()

    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        frmSearch.Show()
        Me.Hide()
        Dim sql As String = "SELECT * FROM Inventory "
        Dim connection As New OleDbConnection(connectionString)
        Dim dataadapter As New OleDbDataAdapter(sql, connection)
        Dim ds As New DataSet
        connection.Open()
        dataadapter.Fill(ds, "Inventory")
        connection.Close()
        frmSearch.dgvProducts.DataSource = ds
        frmSearch.dgvProducts.DataMember = "Inventory"
        frmSearch.dgvProducts.ReadOnly = True
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        frmChangePassword.Show()
        Me.Hide()

    End Sub
End Class